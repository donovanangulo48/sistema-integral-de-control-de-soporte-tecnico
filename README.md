Command line instructions
You can also upload existing files from your computer using the instructions below.


Git global setup
git config --global user.name "donovanangulo48"
git config --global user.email "donovanangulo48@gmail.com"

Create a new repository
git clone https://gitlab.com/donovanangulo48/sistema-integral-de-control-de-soporte-tecnico.git
cd sistema-integral-de-control-de-soporte-tecnico
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push an existing folder
cd existing_folder
git init
git remote add origin https://gitlab.com/donovanangulo48/sistema-integral-de-control-de-soporte-tecnico.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/donovanangulo48/sistema-integral-de-control-de-soporte-tecnico.git
git push -u origin --all
git push -u origin --tags