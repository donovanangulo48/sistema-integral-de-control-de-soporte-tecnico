//Código para Datables

//$('#example').DataTable(); //Para inicializar datatables de la manera más simple


  $(document).ready(function(){
        $('#example').DataTable({
                searchPanes:{
                    cascadePanes:false,
                    dtOpts:{
                        dom:'tp',
                        paging:'true',
                        pagingType:'simple',
                        searching:false
                    }
                },
                dom:'Pfrtip',
                columnDefs:[{
                    searchPanes:{
                        show:false,
                    },
                    targets:[0,1,2,4,7]
                }


                ]


        });

    });

