﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.IO;
using System.Configuration;

namespace Sistema_Control_Soporte.Controllers
{
    public class SoporteController : Controller
    {
        // GET: Soporte
        public ActionResult Index(int tipo = 0)
        {
            if (Session.Count == 0)
            {
                Session["user"] = "";
                Session["pass"] = "";
            }
            if (Session["user"].Equals("") || Session["pass"].Equals("") || Session["user"] == null || Session["pass"] == null)
            {
                ViewBag.error = "Sesion vencida";
                return RedirectToAction("Login");
            }
            else
            {
                Datos_BD cnn = new Datos_BD();
                DataTable dt = null;
                if (tipo == 0)
                {
                    dt = cnn.Consultar_tickets_sin_respuesta();
                }
                if (tipo == 1)
                {
                    dt = cnn.Consultar_tickets_con_respuesta();
                }
                if (tipo == 2)
                {
                    dt = cnn.Consultar_tickets_todos();
                }
                if (tipo == 3)
                {
                    int idAgente = 0;

                    idAgente = int.Parse(cnn.Consultar_agente(Session["user"].ToString()).Rows[0]["id_agente"].ToString());
                    dt = cnn.Consultar_tickets_xagente(idAgente);
                }
                

                List<ticket> lista = new List<ticket>();

                foreach (DataRow r in dt.Rows)
                {
                    ticket t = new ticket();

                    t.id = r["idTicket"].ToString();
                    t.descripcion = r["descripcion"].ToString();
                    t.fechaRegistro = DateTime.Parse(r["fechaRegistro"].ToString());
                    t.horaRegistro = DateTime.Parse(r["horaRegistro"].ToString());
                    t.situacion = r["situacion"].ToString();
                    t.nombreCliente = cnn.Consultar_cliente(int.Parse(r["idCliente"].ToString())).Rows[0]["nombre"].ToString();
                    //falta hacer las consultas al departamento.
                    t.departamento = cnn.Consultar_deptoxticket(int.Parse(r["idTicket"].ToString())).Rows[0]["nombre"].ToString();
                    t.tema = cnn.Consultar_tema(int.Parse(r["idTema"].ToString())).Rows[0]["descripcion"].ToString();
                    t.fuente = cnn.Consultar_fuente(int.Parse(r["idFuente"].ToString())).Rows[0]["nombre"].ToString();

                    lista.Add(t);
                }
                ViewBag.tickets = lista;

                return View();
            }
        }

        // GET: Login
        public ActionResult Login()
        {
            Session["user"] = "";
            Session["pass"] = "";
            return View();
        }

        // GET: Login
        [HttpPost]
        public ActionResult Login(string username, string password)
        {
            Datos_BD cnn = new Datos_BD();

            if (cnn.validarUsuario(username, password))
            {
                Session["user"] = username;
                Session["pass"] = password;

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.error = "Usuario no valido";
                return View();
            }

        }
        // GET: Respuesta
        public ActionResult Respuesta(int idt)
        {
            if (Session.Count == 0)
            {
                Session["user"] = "";
                Session["pass"] = "";
            }
            if (Session["user"].Equals("") || Session["pass"].Equals("") || Session["user"] == null || Session["pass"] == null)
            {
                ViewBag.error = "Sesion vencida";
                return RedirectToAction("Login");
            }
            else
            {
                Datos_BD cnn = new Datos_BD();
                DataTable dt = cnn.Consultar_ticket(idt);

                ticket t = new ticket();

                t.id = idt.ToString();
                t.descripcion = dt.Rows[0]["descripcion"].ToString();
                t.fechaRegistro = DateTime.Parse(dt.Rows[0]["fechaRegistro"].ToString());
                t.horaRegistro = DateTime.Parse(dt.Rows[0]["horaRegistro"].ToString());
                t.situacion = dt.Rows[0]["situacion"].ToString();
                t.nombreCliente = cnn.Consultar_cliente(int.Parse(dt.Rows[0]["idCliente"].ToString())).Rows[0]["nombre"].ToString();

                t.departamento = cnn.Consultar_deptoxticket(int.Parse(dt.Rows[0]["idTicket"].ToString())).Rows[0]["nombre"].ToString();
                t.tema = cnn.Consultar_tema(int.Parse(dt.Rows[0]["idTema"].ToString())).Rows[0]["descripcion"].ToString();
                t.fuente = cnn.Consultar_fuente(int.Parse(dt.Rows[0]["idFuente"].ToString())).Rows[0]["nombre"].ToString();
                string rutaatt = ConfigurationManager.AppSettings["rutaAttach"];
                string rutaWeb = ConfigurationManager.AppSettings["rutaWeb"];
                List<string> listaa = new List<string>();
                if (Directory.Exists(rutaatt + dt.Rows[0]["idCliente"].ToString() + "\\" + dt.Rows[0]["idTicket"].ToString()))
                {
                    string[] archivos = Directory.GetFiles(rutaatt + dt.Rows[0]["idCliente"].ToString() + "\\" + dt.Rows[0]["idTicket"].ToString());

                    foreach (string f in archivos)
                    {
                        FileInfo fi = new FileInfo(f);
                        listaa.Add(rutaWeb + dt.Rows[0]["idCliente"].ToString() + "/" + dt.Rows[0]["idTicket"].ToString() + "/" + fi.Name);
                    }
                    t.Archivos = listaa;
                }

                ViewBag.ticket = t;

                DataTable dtrespuestas = cnn.Consultar_respuestas_x_ticket(idt);

                List<respuesta> respuestas = new List<respuesta>();

                foreach (DataRow r in dtrespuestas.Rows)
                {
                    respuesta tr = new respuesta();
                    tr.contenido = r["contenido"].ToString();
                    tr.id = idt.ToString();
                    tr.Estado = r["Estado"].ToString();
                    tr.Fecha = DateTime.Parse(r["Fecha_respuesta"].ToString());
                    tr.Hora = DateTime.Parse(r["Hora_respuesta"].ToString());
                    tr.contador = r["contador_respuesta"].ToString();
                    if (r["esCliente"].ToString().Equals("0"))
                    {
                        tr.escliente = false;
                        tr.Agente = cnn.Consultar_agentexid(int.Parse(r["id_agente"].ToString())).Rows[0]["nombre"].ToString() + " " + cnn.Consultar_agentexid(int.Parse(r["id_agente"].ToString())).Rows[0]["apellidos"].ToString();
                    }
                    else
                    {
                        tr.escliente = true;
                        tr.Agente = cnn.Consultar_cliente(int.Parse(r["id_agente"].ToString())).Rows[0]["nombre"].ToString();
                    }

                    if (Directory.Exists(rutaatt + dt.Rows[0]["idCliente"].ToString() + "\\" + dt.Rows[0]["idTicket"].ToString() + "\\" + tr.contador) )
                    {
                        string[] archivos = Directory.GetFiles(rutaatt + dt.Rows[0]["idCliente"].ToString() + "\\" + dt.Rows[0]["idTicket"].ToString() + "\\" + tr.contador);

                        foreach (string f in archivos)
                        {
                            FileInfo fi = new FileInfo(f);
                            t.Archivos.Add(rutaWeb + dt.Rows[0]["idCliente"].ToString() + "/" + dt.Rows[0]["idTicket"].ToString() + "/" + tr.contador +"/" + fi.Name);
                        }
                    }

                    respuestas.Add(tr);

                }

                ViewBag.repuestas = respuestas;
                return View();
            }
        }

        // GET: Solicitud
        public ActionResult Solicitud()
        {
            Datos_BD cnn = new Datos_BD();

            ViewBag.Message = "";
            DataTable dtdeptos = cnn.Consultar_departamentos();

            List<string> listad = new List<string>();
            foreach (DataRow r in dtdeptos.Rows)
            {
                listad.Add(r["nombre"].ToString());
            }

            ViewBag.listad = listad;

            return View();
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Solicitud(string Asunto, string Nombre, string Email, string departamento,  string descripcion, HttpPostedFileBase uploadFile)
        {
            ViewBag.Message = "";
              Datos_BD cnn = new Datos_BD();
            
            try
            {
                int idCliente = 0;
                DataTable cliente = cnn.Consultar_clientexmail(Email);
                if (cliente.Rows.Count == 0)
                {
                    cnn.Agregar_cliente(Nombre, Email, "0");

                }
                cliente = cnn.Consultar_clientexmail(Email);

                DataTable depts = cnn.Consultar_departamentos();
                int idd = 1;
                foreach (DataRow r in depts.Rows)
                {
                    if (r["nombre"].ToString().Equals(departamento))
                    {
                        idd = int.Parse(r["idDepartamento"].ToString());
                    }
                }
                
                DataTable resp = cnn.Agregar_ticket(descripcion, Asunto, int.Parse(cliente.Rows[0]["idCliente"].ToString()), 1, 2, idd);
                int id_ticket = int.Parse(resp.Rows[0][0].ToString());

                string rutaatt = ConfigurationManager.AppSettings["rutaAttach"];
                if (uploadFile != null && uploadFile.ContentLength > 0)
                    try
                    {
                        if (!Directory.Exists(rutaatt + "/" + cliente.Rows[0]["idCliente"].ToString() + "/" + id_ticket.ToString() +"/"))
                        {
                            Directory.CreateDirectory(rutaatt + "/" + cliente.Rows[0]["idCliente"].ToString() + "/" + id_ticket.ToString() + "/");
                        }
                        else
                        {
                            Directory.Delete(rutaatt + "/" + cliente.Rows[0]["idCliente"].ToString() + "/" + id_ticket.ToString() + "/", true);
                            Directory.CreateDirectory(rutaatt + "/" + cliente.Rows[0]["idCliente"].ToString() + "/" + id_ticket.ToString() + "/");
                        }
                        string path = Path.Combine(rutaatt + "/" + cliente.Rows[0]["idCliente"].ToString() + "/" + id_ticket.ToString() + "/",
                                                   Path.GetFileName(uploadFile.FileName));
                        uploadFile.SaveAs(path);

                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                else
                {

                }


                try
                {
                    cnn.enviarmail(cliente.Rows[0]["email"].ToString(), idd, true, "Se ha registrado un nuevo caso al sistema de soporte Técnico. Se le ha asignado el caso numero "+ id_ticket.ToString(), Asunto, id_ticket);
                    cnn.enviarmail(cliente.Rows[0]["email"].ToString(), idd, false, "Se ha creado un nuevo caso. Por favor ingresar al dashbord", Asunto, id_ticket);
                }
                catch (Exception ex)
                {
                    cnn.Log("Error : " + ex.Message.ToString());
                }
                
                //falta enviar por correo el caso
                ViewBag.Message = "Caso agregado correctamente";
            }
            catch (Exception ex)
            {
                ViewBag.Message = "ERROR:" + ex.Message.ToString();
            };
            
            DataTable dtdeptos = cnn.Consultar_departamentos();

            List<string> listad = new List<string>();
            foreach (DataRow r in dtdeptos.Rows)
            {
                listad.Add(r["nombre"].ToString());
            }

            ViewBag.listad = listad;
            
            return View();
        }
        [HttpPost, ValidateInput(false)]
        public ActionResult Respuesta(string descripcion, string id_ticket)
        {
            Datos_BD cnn = new Datos_BD();
            ViewBag.Message = "";
            int idAgente = 0;
            DataTable agente = cnn.Consultar_agente(Session["user"].ToString());
            idAgente = int.Parse(agente.Rows[0]["id_agente"].ToString());
            int iddepto = 0;
            iddepto = int.Parse(agente.Rows[0]["idDepartamento"].ToString());
            try
            {
                if (cnn.Consultar_agentexticket(int.Parse(id_ticket)).Rows.Count == 0)
                {
                    cnn.Asignar_ticket(int.Parse(id_ticket), idAgente);
                }
                cnn.Agregar_respuesta_agente(id_ticket, descripcion, idAgente, 0);
                DataTable cliente = cnn.Consultar_cliente(int.Parse(cnn.Consultar_ticket(int.Parse(id_ticket)).Rows[0]["idCliente"].ToString()));
                string mensaje = cnn.Consultar_ticket(int.Parse(id_ticket)).Rows[0]["situacion"].ToString();
                cnn.enviarmail(cliente.Rows[0]["email"].ToString(), iddepto, true, descripcion + "<br> Atentamente, " + agente.Rows[0]["nombre"].ToString(), mensaje , int.Parse(id_ticket));
                ViewBag.Message = "Agregado correctamente";
            }
            catch (Exception ex)
            {
                ViewBag.Message = "Error: " + ex.Message.ToString();
                cnn.Log("Error: " + ex.Message);
            }
            return RedirectToAction("Respuesta", "Soporte", new { idt = id_ticket });
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Finalizar(string id_ticket)
        {
            Datos_BD cnn = new Datos_BD();
            try
            {
                cnn.Finalizar(int.Parse(id_ticket));
            }
            catch (Exception ex){
                cnn.Log("Error: " + ex.Message.ToString());
            }
            return RedirectToAction("Index", "Soporte");
        }
    }
    public class ticket
    {
        public string id { get; set; }
        public string descripcion { get; set; }
        public DateTime fechaRegistro { get; set; }
        public DateTime horaRegistro { get; set; }
        public string situacion { get; set; }
        public string nombreCliente { get; set; }
        public string tema { get; set; }
        public string fuente { get; set; }
        public string departamento { get; set; }

        public List<string> Archivos { get; set; }

    }
    public class respuesta
    {
        public string id { get; set; }
        public string contenido { get; set; }
        public string contador { get; set; }
        public DateTime Fecha { get; set; }
        public string Agente { get; set; }
        public DateTime Hora { get; set; }
        public string Estado { get; set; }
        public bool escliente { get; set; }

        public List<string> Archivos { get; set; }
    }
}