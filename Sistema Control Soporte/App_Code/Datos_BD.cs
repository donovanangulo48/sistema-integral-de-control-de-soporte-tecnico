﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.IO;

namespace Sistema_Control_Soporte
{
    public class Datos_BD
    {
        MySqlConnection conexion;


        public Datos_BD()
        {
            String cnn = ConfigurationManager.AppSettings["cnn"];
            conexion = new MySqlConnection(cnn);
        }


        public DataTable Consultar_agente(string mail)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_agente_consultar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dagente", mail));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }
        public DataTable Consultar_agentexid(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_agente_consultarxid ", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dagente", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public bool validarUsuario(string correo, string clave)
        {
            bool res = false;
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("SP_VALIDAR_AGENTE", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("corr", correo));
            comm.Parameters.Add(new MySqlParameter("clave", Encriptar(clave)));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();
            if (dt.Rows.Count > 0)
            {
                res = true;
            }

            return res;
        }

        public DataTable Consultar_cliente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_cliente_consultar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }
        public DataTable Consultar_clientexmail(string mail)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_cliente_consultarxmail ", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("email", mail));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }
        public DataTable Consultar_tema(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_sel_tema ", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("id", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }
        public DataTable Consultar_fuente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_sel_fuente ", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("id", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }
        public DataTable Consultar_deptoxticket(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_sel_departamentoxticket", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("id", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_desempeño(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_desempeno_consultar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_reporte_general_tickets(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_reportegeneraltickets_consultar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dGrafico", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_reporte_tickets_agente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_reporteticketsagente_consultar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dagente", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_respuesta_agente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_respuesta_agente_consultar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dticket", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_tickets(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_consultar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }
        public DataTable Consultar_ticket(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_sel_ticket", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("id", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        //****************************************************************************************//

        public void Agregar_agente(int id, string nombre, string apellidos, string direccion, int telefono, string correo, string clave, string nivel, int horas_laboradas, int id_departamento)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_agente_insertar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dagente", id));
            comm.Parameters.Add(new MySqlParameter("dnombre", nombre));
            comm.Parameters.Add(new MySqlParameter("dapellidos", apellidos));
            comm.Parameters.Add(new MySqlParameter("ddireccion", direccion));
            comm.Parameters.Add(new MySqlParameter("dtelefono", telefono));
            comm.Parameters.Add(new MySqlParameter("dcorreo", correo));
            comm.Parameters.Add(new MySqlParameter("dclave", clave));
            comm.Parameters.Add(new MySqlParameter("dnivel", nivel));
            comm.Parameters.Add(new MySqlParameter("dhoras", horas_laboradas));
            comm.Parameters.Add(new MySqlParameter("ddepartamento", id_departamento));
            comm.ExecuteNonQuery();
            conexion.Close();
        }


        public void Agregar_cliente(string nombre, string email, string organizacion)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_cliente_insertar", conexion);
            comm.CommandType = CommandType.StoredProcedure;

            comm.Parameters.Add(new MySqlParameter("dnombre", nombre));
            comm.Parameters.Add(new MySqlParameter("demail", email));
            comm.Parameters.Add(new MySqlParameter("dorganizacion", organizacion));
            comm.ExecuteNonQuery();
            conexion.Close();
        }
        public void Finalizar(int id)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_upd_ticket_finalizar", conexion);
            comm.CommandType = CommandType.StoredProcedure;

           
            comm.Parameters.Add(new MySqlParameter("id", id));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Agregar_desempeño(string servicio, string descripcion, int calificacion, string pregunta1, string pregunta2, string pregunta3, string pregunta4, int id_cliente)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_desempeno_insertar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("servicio", servicio));
            comm.Parameters.Add(new MySqlParameter("ddescripcion", descripcion));
            comm.Parameters.Add(new MySqlParameter("dcalificacion", calificacion));
            comm.Parameters.Add(new MySqlParameter("dpregunta1", pregunta1));
            comm.Parameters.Add(new MySqlParameter("dpregunta2", pregunta2));
            comm.Parameters.Add(new MySqlParameter("dpregunta3", pregunta3));
            comm.Parameters.Add(new MySqlParameter("dpregunta4", pregunta4));
            comm.Parameters.Add(new MySqlParameter("dcliente", id_cliente));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Agregar_reporte_general_tickets(int creados, int dcreados, int cerrados, int reabiertos, int asignados)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_reportegeneraltickets_insertar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("creados", creados));
            comm.Parameters.Add(new MySqlParameter("dcreados", dcreados));
            comm.Parameters.Add(new MySqlParameter("dcerrados", cerrados));
            comm.Parameters.Add(new MySqlParameter("dreabiertos", reabiertos));
            comm.Parameters.Add(new MySqlParameter("dasignados", asignados));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Agregar_reporte_tickets_agente(int abiertos, int atrasados, int cerrados, int reabiertos, int tiempo_servicio, int tiempo_respuesta, int departamento, int agente)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_reporteticketsagentes_insertar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dabiertos", abiertos));
            comm.Parameters.Add(new MySqlParameter("datrasados", atrasados));
            comm.Parameters.Add(new MySqlParameter("dcerrados", cerrados));
            comm.Parameters.Add(new MySqlParameter("dreabiertos", reabiertos));
            comm.Parameters.Add(new MySqlParameter("dtiempos", tiempo_servicio));
            comm.Parameters.Add(new MySqlParameter("dtiempor", tiempo_respuesta));
            comm.Parameters.Add(new MySqlParameter("ddepartamento", departamento));
            comm.Parameters.Add(new MySqlParameter("dagente", agente));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public DataTable Agregar_respuesta_agente(string id_ticket, string contenido, int agente, int esCliente)
        {
            DataTable dt = new DataTable();
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_respuesta_agente_insertar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dticket", id_ticket));
            comm.Parameters.Add(new MySqlParameter("dcontenido", contenido));
            comm.Parameters.Add(new MySqlParameter("dagente", agente));
            comm.Parameters.Add(new MySqlParameter("tipo", esCliente));
            MySqlDataReader re = comm.ExecuteReader();
            dt.Load(re);
            conexion.Close();
            return dt;
        }


        public void Agregar_tema(int id_tema, string descripcion)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tema_insertar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dtema", id_tema));
            comm.Parameters.Add(new MySqlParameter("ddescripcion", descripcion));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Asignar_ticket(int idticket, int idagente)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_asignar_ticket", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("idticket", idticket));
            comm.Parameters.Add(new MySqlParameter("idagente", idagente));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public DataTable Agregar_ticket(string descripcion, string situacion, int cliente, int tema, int fuente, int departamento)
        {
            DataTable dt = new DataTable();
            try
            {
                conexion.Open();
                MySqlCommand comm = new MySqlCommand("proc_tickets_insertar", conexion);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.Add(new MySqlParameter("ddescripcion", descripcion));

                comm.Parameters.Add(new MySqlParameter("situacion", situacion));
                comm.Parameters.Add(new MySqlParameter("dcliente", cliente));
                comm.Parameters.Add(new MySqlParameter("dTema", tema));
                comm.Parameters.Add(new MySqlParameter("dfuente", fuente));
                comm.Parameters.Add(new MySqlParameter("ddepartamento", departamento));
                MySqlDataReader re = comm.ExecuteReader();
                dt.Load(re);
                conexion.Close();


            }
            finally
            {
                conexion.Close();
            }
            return dt;

        }


        //****************************************************************************************//

        public void Actualizar_agente(int id, string nombre, string apellidos, string direccion, int telefono, string correo, string clave, string nivel, int horas_laboradas, int id_departamento)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_agente_actualizar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dagente", id));
            comm.Parameters.Add(new MySqlParameter("dnombre", nombre));
            comm.Parameters.Add(new MySqlParameter("dapellidos", apellidos));
            comm.Parameters.Add(new MySqlParameter("ddireccion", direccion));
            comm.Parameters.Add(new MySqlParameter("dtelefono", telefono));
            comm.Parameters.Add(new MySqlParameter("dcorreo", correo));
            comm.Parameters.Add(new MySqlParameter("dclave", clave));
            comm.Parameters.Add(new MySqlParameter("dnivel", nivel));
            comm.Parameters.Add(new MySqlParameter("dhoras", horas_laboradas));
            comm.Parameters.Add(new MySqlParameter("ddepartamento", id_departamento));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Actualizar_cliente(int id, string nombre, string email, string organizacion)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_cliente_actualizar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dcliente", id));
            comm.Parameters.Add(new MySqlParameter("dnombre", nombre));
            comm.Parameters.Add(new MySqlParameter("demail", email));
            comm.Parameters.Add(new MySqlParameter("dorganizacion", organizacion));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Actualizar_reporte_tickets_agente(int abiertos, int atrasados, int cerrados, int reabiertos, int tiempo_servicio, int tiempo_respuesta, int departamento, int agente)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_reporteticketsagente_actualizar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dagente", agente));
            comm.Parameters.Add(new MySqlParameter("dabiertos", abiertos));
            comm.Parameters.Add(new MySqlParameter("datrasados", atrasados));
            comm.Parameters.Add(new MySqlParameter("dcerrados", cerrados));
            comm.Parameters.Add(new MySqlParameter("dreabiertos", reabiertos));
            comm.Parameters.Add(new MySqlParameter("dtiempos", tiempo_servicio));
            comm.Parameters.Add(new MySqlParameter("dtiempor", tiempo_respuesta));
            comm.Parameters.Add(new MySqlParameter("ddepartamento", departamento));

            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Actualizar_respuesta_agente(int id_ticket, int estado)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_respuesta_agente_actualizar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dticket", id_ticket));
            comm.Parameters.Add(new MySqlParameter("destado", estado));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Actualizar_tema(int id_tema, string descripcion)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tema_actualizar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dtema", id_tema));
            comm.Parameters.Add(new MySqlParameter("ddescripcion", descripcion));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Actualizar_ticket(int ticket, string situacion)
        {
            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_actualizar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dticket", ticket));
            comm.Parameters.Add(new MySqlParameter("dsituacion", situacion));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        //****************************************************************************************//

        public void Eliminar_agente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_agente_eliminar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dagente", id));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Eliminar_cliente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_cliente_eliminar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", id));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Eliminar_desempeño(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_desempeno_eliminar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", id));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Eliminar_reporte_general_tickets(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_reportegeneraltickets_eliminar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dGrafico", id));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Eliminar_reporte_tickets_agente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_reporteticketsagente_eliminar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dagente", id));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Eliminar_respuesta_agente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_respuesta_agente_eliminar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", id));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        public void Eliminar_tickets(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_eliminar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dticket", id));
            comm.ExecuteNonQuery();
            conexion.Close();
        }

        //****************************************************************************************//

        public DataTable Consultar_agente_x_departamento(string departamento)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_agente_x_departamento", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", departamento));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_reporte_x_agente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_reporte_x_agente", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_respuestas_x_ticket(int id_ticket)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_respuestas_ticket", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", id_ticket));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_tickets_con_respuesta()
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_con_respuesta", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_tickets_sin_respuesta()
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_sin_respuesta", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_tickets_x_fuente(string fuente)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_x_fuente", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", fuente));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_tickets_x_situacion(string situacion)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_x_situacion", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", situacion));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_tickets_x_tema(string tema)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_x_tema", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("dato", tema));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public DataTable Consultar_tickets_xagente(int id)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_agente", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("idagente", id));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }
        public DataTable Consultar_tickets_todos()
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_tickets_todos", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }
        public DataTable Consultar_agentexticket(int idt)
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_agentexticket", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.Parameters.Add(new MySqlParameter("idticket", idt));
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }


        public DataTable Consultar_fuentes()
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_fuente_consultar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }
        public DataTable Consultar_mails()
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_sel_deptocorreo", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }


        public DataTable Consultar_departamentos()
        {

            conexion.Open();
            MySqlCommand comm = new MySqlCommand("proc_departamento_consultar", conexion);
            comm.CommandType = CommandType.StoredProcedure;
            comm.ExecuteNonQuery();
            MySqlDataAdapter da = new MySqlDataAdapter(comm);
            DataTable dt = new DataTable();
            da.Fill(dt);
            conexion.Close();

            return dt;
        }

        public string Encriptar(string _cadenaAencriptar)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadenaAencriptar);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        public string DesEncriptar(string _cadenaAdesencriptar)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(_cadenaAdesencriptar);
            //result = System.Text.Encoding.Unicode.GetString(decryted, 0, decryted.ToArray().Length);
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }




        public void enviarmail(string destinatario, int id_departmento, bool escliente, string mensaje, string Asunto, int idtickect)
        {
            System.Net.Mail.MailMessage mmsg = new System.Net.Mail.MailMessage();

            if (escliente)
            {
                mmsg.Subject = "Ticket[" + idtickect.ToString() + "] " + Asunto;
            }
            else
            {
                mmsg.Subject = "Caso asignado :" + idtickect.ToString() + " " + Asunto;
            }

            mmsg.SubjectEncoding = System.Text.Encoding.UTF8;


            mmsg.Body = mensaje;
            mmsg.BodyEncoding = System.Text.Encoding.UTF8;
            mmsg.IsBodyHtml = true;




            System.Net.Mail.SmtpClient cliente = new System.Net.Mail.SmtpClient();




            DataTable mails = Consultar_mails();
            foreach (DataRow r in mails.Rows)
            {
                if (int.Parse(r["idDepartamento"].ToString()) == id_departmento)
                {
                    cliente.Port = int.Parse(r["SMTPPort"].ToString());
                    cliente.EnableSsl = false;


                    cliente.Host = r["SMTPServer"].ToString();
                    cliente.Credentials = new System.Net.NetworkCredential(r["email"].ToString(), r["password"].ToString());
                    mmsg.From = new System.Net.Mail.MailAddress(r["email"].ToString());
                }
            }

            try
            {
                if (escliente)
                {
                    mmsg.To.Add(destinatario);
                    cliente.Send(mmsg);
                }
                else
                {
                    foreach (DataRow agen in Consultar_agente_x_departamento(id_departmento.ToString()).Rows)
                    {
                        mmsg.To.Add(agen["correo"].ToString());
                        cliente.Send(mmsg);
                    }
                }

            }
            catch (Exception ex)
            {
                Log("Error : " + ex.Message.ToString());
            }
        }

        public void Log(string logMessage)
        {
            try
            {
                string filepath = ConfigurationManager.AppSettings["logruta"] + "LOG_" + DateTime.Now.Day.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString() + ".txt";
                StreamWriter w;
                if (File.Exists(filepath))
                {
                    w = File.AppendText(filepath);
                }
                else
                {
                    w = File.CreateText(filepath);
                }

                w.Write("\r\nLog Entry : ");
                w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                w.WriteLine("  :");
                w.WriteLine($"  :{logMessage}");
                w.WriteLine("-------------------------------");
                w.Close();
            }
            finally
            {

            }


        }

    }
}