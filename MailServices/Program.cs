﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net;
using MailKit.Net.Pop3;
using MimeKit;
using MailServices;
using System.Data;
using System.Configuration;
using System.IO;

namespace MailServices
{
    class Program
    {
        static void Main(string[] args)
        {
            Datos_BD cnn = new Datos_BD();

            foreach (DataRow r in cnn.Consultar_mails().Rows)
            {
                try
                {
                    using (Pop3Client pop3 = new Pop3Client())
                    {
                        pop3.Connect(r["servidor"].ToString(), int.Parse(r["puerto"].ToString()), MailKit.Security.SecureSocketOptions.None);  // or ConnectSSL for SSL/TLS     
                        pop3.Authenticate(r["email"].ToString(), r["password"].ToString());

                        for (int i = 0; i < pop3.Count; i++)
                        {
                            try
                            {
                                var eml = pop3.GetMessage(i);

                                DataTable cliente = cnn.Consultar_clientexmail(eml.From.Mailboxes.ToList()[0].Address);

                                if (cliente.Rows.Count == 0)
                                {
                                    //falta seleccionar la organizacion
                                    cnn.Agregar_cliente(eml.From.Mailboxes.ToList()[0].Name, eml.From.Mailboxes.ToList()[0].Address, "0");
                                    cliente = cnn.Consultar_clientexmail(eml.From.Mailboxes.ToList()[0].Address);
                                }
                                int idt = 0;
                                if (eml.Subject.Contains("Ticket["))
                                {
                                    idt = int.Parse(eml.Subject.Split('[')[1].Split(']')[0]);
                                }
                                if (idt != 0)
                                {
                                    DataTable idcaso = cnn.Agregar_respuesta_agente(idt.ToString(), eml.HtmlBody , int.Parse( cliente.Rows[0]["idCliente"].ToString()), 1);
                                    string rutaattac = ConfigurationManager.AppSettings["rutaAttach"];
                                    foreach (var attachment in eml.Attachments)
                                    {
                                        Directory.CreateDirectory(rutaattac + cliente.Rows[0]["idCliente"].ToString() + "\\" + idcaso.Rows[0][0].ToString() + "\\");
                                        if (attachment is MessagePart)
                                        {

                                            var part = (MessagePart)attachment;
                                            var stream = File.Create(rutaattac + cliente.Rows[0]["idCliente"].ToString() + "\\" + idt +"\\" + idcaso.Rows[0][0].ToString() + "\\" + attachment.ContentDisposition?.FileName);

                                            part.Message.WriteTo(stream);
                                        }
                                        else
                                        {
                                            var part = (MimePart)attachment;
                                            var stream = File.Create(rutaattac + cliente.Rows[0]["idCliente"].ToString() + "\\" + idt +"\\" + idcaso.Rows[0][0].ToString() + "\\" + part.FileName);


                                            part.Content.DecodeTo(stream);
                                        }

                                    }
                                    try
                                    {
                                        cnn.enviarmail(eml.From.Mailboxes.ToList()[0].Address, int.Parse(r["idDepartamento"].ToString()), false, "Se ha registrado un nuevo caso.", eml.Subject, idt);
                                    }catch (Exception ex)
                                    {
                                        cnn.Log("Error : " + ex.Message.ToString());
                                    }
                                    
                                }
                                else
                                {
                                    DataTable idcaso = cnn.Agregar_ticket(eml.HtmlBody, eml.Subject, int.Parse(cliente.Rows[0]["idCliente"].ToString()), 1, 1, int.Parse(r["idDepartamento"].ToString()));
                                    string rutaattac = ConfigurationManager.AppSettings["rutaAttach"];
                                    foreach (var attachment in eml.Attachments)
                                    {
                                        Directory.CreateDirectory(rutaattac + cliente.Rows[0]["idCliente"].ToString() + "\\" + idcaso.Rows[0][0].ToString() + "\\");
                                            if (attachment is MessagePart)
                                            {

                                                var part = (MessagePart)attachment;
                                                var stream = File.Create(rutaattac + cliente.Rows[0]["idCliente"].ToString() + "\\" + idcaso.Rows[0][0].ToString() + "\\" + attachment.ContentDisposition?.FileName);

                                                part.Message.WriteTo(stream);
                                            }
                                            else
                                            {
                                                var part = (MimePart)attachment;
                                                var stream = File.Create(rutaattac + cliente.Rows[0]["idCliente"].ToString() + "\\" + idcaso.Rows[0][0].ToString() + "\\" + part.FileName);


                                                part.Content.DecodeTo(stream);
                                            }
                                        
                                    }
                                    try { 
                                        cnn.enviarmail(eml.From.Mailboxes.ToList()[0].Address, int.Parse(r["idDepartamento"].ToString()), true, "Se ha registrado un nuevo caso al sistema de soporte Técnico. Se le ha asignado el caso numero " + idcaso.Rows[0][0].ToString(), eml.Subject, int.Parse(idcaso.Rows[0][0].ToString()));
                                        cnn.enviarmail(eml.From.Mailboxes.ToList()[0].Address, int.Parse(r["idDepartamento"].ToString()), false, "Se ha creado un nuevo caso. Por favor ingresar al dashboard", eml.Subject, int.Parse(idcaso.Rows[0][0].ToString()));
                                    }
                                    catch (Exception ex)
                                    {
                                        cnn.Log("Error : " + ex.Message.ToString());
                                    }
                                }
                                pop3.DeleteMessage(i);
                            }
                            catch (Exception ex)
                            {
                                cnn.Log("Error: " + ex.Message.ToString());
                            }
                        }
                        pop3.Disconnect(true);
                    }
                }
                catch (Exception ex) {
                    cnn.Log("Error: " + ex.Message.ToString());
                }
            }

            


        }
    }
}
